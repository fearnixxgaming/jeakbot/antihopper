package de.fearnixx.jeak.antihopping;

import de.fearnixx.jeak.antihopping.event.IClientCaughtEvent;
import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.reflect.LocaleUnit;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.query.IQueryConnection;
import de.mlessmann.confort.api.IConfigNode;
import de.mlessmann.confort.api.IValueHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class PunishmentWatcher {

    private static final Logger logger = LoggerFactory.getLogger(PunishmentWatcher.class);

    @Inject
    private IServer server;

    @Inject
    private IDataCache dataCache;

    @Inject
    @LocaleUnit(value = "anti-hopping", defaultResource = "antihopping/lang.json")
    private ILocalizationUnit localization;

    private Map<String, LocalDateTime> joinNotifyThreshold = new ConcurrentHashMap<>();

    private final List<Integer> assignServerGroups = new ArrayList<>();
    private final List<Integer> notifyGroups = new ArrayList<>();
    private int jailChannelId;

    public void loadConfiguration(IConfigNode config) {
        final String penaltyNodeName = "penalty";
        jailChannelId = config.getNode(penaltyNodeName, "move-to-channel").optInteger(0);

        config.getNode(penaltyNodeName, "assign", "server-groups")
                .optList()
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(IConfigNode::isPrimitive)
                .map(IValueHolder::asInteger)
                .forEach(assignServerGroups::add);
        config.getNode(penaltyNodeName, "notify-groups").optList()
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(IConfigNode::isPrimitive)
                .map(IValueHolder::asInteger)
                .forEach(notifyGroups::add);
    }

    @Listener
    public void onClientCaught(IClientCaughtEvent event) {
        IClient client = event.getTarget();
        IQueryConnection connection = server.getConnection();

        if (!assignServerGroups.isEmpty()) {
            assignServerGroups.stream()
                    .map(client::addServerGroup)
                    .forEach(connection::sendRequest);
        }

        if (jailChannelId > 0) {
            connection.sendRequest(client.moveToChannel(jailChannelId));
        }

        Map<String, String> params = new HashMap<>();
        params.put("target", client.getNickName());
        params.put("target_uid", client.getClientUniqueID());
        params.put("points", Integer.toString(event.reachedPoints()));
        params.put("threshold", Integer.toString(event.triggerThreshold()));

        if (!notifyGroups.isEmpty()) {
            notifyStaff(params, "caught.staff");
        }

        String caughtMessage = localization.getContext(client.getCountryCode())
                .optMessage("caught.target", params)
                .orElse("You have been caught channel hopping!");
        connection.sendRequest(client.sendMessage(caughtMessage));
    }

    @Listener
    public void onClientEnter(IQueryEvent.INotification.IClientEnter event) {
        IClient target = event.getTarget();
        List<Integer> groupIDs = target.getGroupIDs();

        if (!assignServerGroups.isEmpty() && groupIDs.containsAll(assignServerGroups) && jailChannelId > 0) {
            logger.debug("Moving jailed client back to jail after join. {}", target);
            server.getConnection()
                    .sendRequest(target.moveToChannel(jailChannelId));

            Map<String, String> params = new HashMap<>();
            params.put("target", target.getNickName());
            params.put("target_uid", target.getClientUniqueID());

            String targetMsg = localization.getContext(target.getCountryCode())
                    .optMessage("rejoin.target", params)
                    .orElse("You have re-joined still caught from channel hopping.");
            server.getConnection()
                    .sendRequest(target.sendMessage(targetMsg));

            LocalDateTime renotifyThreshold = LocalDateTime.now().minusMinutes(5);
            joinNotifyThreshold
                    .entrySet()
                    .removeIf(e -> e.getValue().isBefore(renotifyThreshold));

            if (!joinNotifyThreshold.containsKey(target.getClientUniqueID())) {
                notifyStaff(params, "rejoin.staff");
                joinNotifyThreshold.put(target.getClientUniqueID(), LocalDateTime.now());
            }
        }
    }

    private void notifyStaff(Map<String, String> params, String messageId) {
        dataCache.getClients()
                .stream()
                .filter(someClient -> notifyGroups.stream()
                        .anyMatch(someClient.getGroupIDs()::contains))
                .forEach(staffClient -> {
                    String msg = localization.getContext(staffClient.getCountryCode())
                            .optMessage(messageId, params)
                            .orElse("A client triggered the AntiHopping mechanism!");
                    server.getConnection().sendRequest(staffClient.sendMessage(msg));
                });
    }
}
