package de.fearnixx.jeak.antihopping.event;

import de.fearnixx.jeak.event.ITargetClient;

public interface IClientCaughtEvent extends ITargetClient {

    /**
     * Number of points the client collected to trigger the detection.
     */
    int reachedPoints();

    /**
     * Applied hopping-threshold for this client.
     */
    int triggerThreshold();
}
