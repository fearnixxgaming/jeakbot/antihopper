package de.fearnixx.jeak.antihopping.event;

import de.fearnixx.jeak.teamspeak.data.IClient;

public class ClientCaughtEvent implements IClientCaughtEvent {

    private final int pointsReached;
    private final int pointThreshold;
    private final IClient target;

    public ClientCaughtEvent(int pointsReached, int pointThreshold, IClient target) {
        this.pointsReached = pointsReached;
        this.pointThreshold = pointThreshold;
        this.target = target;
    }

    @Override
    public int reachedPoints() {
        return pointsReached;
    }

    @Override
    public int triggerThreshold() {
        return pointThreshold;
    }

    @Override
    public IClient getTarget() {
        return target;
    }
}
