package de.fearnixx.jeak.antihopping;

import de.fearnixx.jeak.antihopping.event.ClientCaughtEvent;
import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.service.task.ITask;
import de.fearnixx.jeak.service.task.ITaskService;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.mlessmann.confort.api.IConfigNode;
import de.mlessmann.confort.api.IValueHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class AntiHopper {

    private static final Logger logger = LoggerFactory.getLogger(AntiHopper.class);

    @Inject
    public ITaskService taskService;

    @Inject
    public IDataCache dataCache;

    @Inject
    private IEventService eventService;

    private ITask workerTask = ITask.builder()
            .name("AntiHopper-Queue")
            .interval(5, TimeUnit.SECONDS)
            .runnable(this::doWork)
            .build();

    private Map<Integer, AtomicInteger> tracker = new ConcurrentHashMap<>();
    private List<Integer> ignoredGroups = new ArrayList<>();
    private Integer maxThreshold;
    private Integer pointsPerMove;

    public void loadConfig(IConfigNode config) {
        maxThreshold = config.getNode("max-threshold").optInteger(8);
        pointsPerMove = config.getNode("per-move").optInteger(2);

        config.getNode("ignore-groups")
                .optList()
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(IConfigNode::isPrimitive)
                .map(IValueHolder::asInteger)
                .forEach(ignoredGroups::add);

        tracker.clear();
        taskService.scheduleTask(workerTask);
    }

    private void doWork() {
        tracker.values()
                .stream()
                .filter(c -> c.get() > 4)
                .forEach(c -> c.addAndGet(-5));
    }

    private void trigger(Integer clientID) {
        AtomicInteger currentScore = tracker.get(clientID);
        IClient client = dataCache.getClientMap().getOrDefault(clientID, null);
        if (client == null) {
            logger.warn("Cannot punish client with ID {}: Client not found!", clientID);
            return;
        }

        ClientCaughtEvent event = new ClientCaughtEvent(currentScore.get(), maxThreshold, client);
        eventService.fireEvent(event);
        currentScore.set(0);
    }

    /**
     * Increases the counter for a client that moved - by themselves - from one channel to another by the configured amount
     */
    @Listener
    public void onClientMove(IQueryEvent.INotification.IClientMoved event) {

        // When client did not jump by himself, do nothing
        Optional<String> reasonID = event.getProperty("reasonid");
        if (!reasonID.isPresent() || !"0".equals(reasonID.get())) {
            logger.debug("Not tracking movement - forced: {}", event.getTarget());
            return;
        }

        boolean ignored = event.getTarget().getGroupIDs()
                .stream()
                .anyMatch(gId -> ignoredGroups.contains(gId));

        // When client is member of an ignored group, do nothing
        if (ignored) {
            logger.debug("Not tracking client - ignored: {}", event.getTarget());
            return;
        }

        Integer clientID = event.getTarget().getClientID();
        AtomicInteger counter = tracker.computeIfAbsent(clientID, id -> new AtomicInteger(0));

        Integer result = counter.addAndGet(pointsPerMove);
        if (result > maxThreshold) {
            trigger(clientID);
        }
    }

    @Listener
    public void onClientLeave(IQueryEvent.INotification.IClientLeave event) {
        tracker.remove(Integer.valueOf(event.getProperty("clid").get()));
    }
}
