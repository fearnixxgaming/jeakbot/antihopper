package de.fearnixx.jeak.antihopping;

import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.*;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.util.Configurable;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;

@JeakBotPlugin(id = "antihopping")
public class AntiHoppingPlugin extends Configurable {

    private static final String DEFAULT_CONF_URI = "/antihopping/defaultConf.json";

    @Inject
    @Config
    private IConfig configRef;

    private AntiHopper antiHopper = new AntiHopper();
    private PunishmentWatcher punishmentWatcher = new PunishmentWatcher();

    @Inject
    private IInjectionService injectionService;

    @Inject
    private IEventService eventService;

    public AntiHoppingPlugin() {
        super(AntiHoppingPlugin.class);
    }

    @Override
    protected IConfig getConfigRef() {
        return configRef;
    }

    @Override
    protected String getDefaultResource() {
        return DEFAULT_CONF_URI;
    }

    @Override
    protected boolean populateDefaultConf(IConfigNode root) {
        return false;
    }

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {
        if (!loadConfig()) {
            event.cancel();
        } else {
            injectionService.injectInto(punishmentWatcher);
            punishmentWatcher.loadConfiguration(getConfig());

            injectionService.injectInto(antiHopper);
            antiHopper.loadConfig(getConfig());

            eventService.registerListeners(punishmentWatcher, antiHopper);
        }
    }

    @Override
    protected void onDefaultConfigLoaded() {
        saveConfig();
    }
}
